public class Kennel {

    public DogBean[] buildDogs() {

        DogBean[] DogBeans = new DogBean[4];
        DogBeans[0] = new DogBean(2, "Green", 6.3, "Collie","Bob");
        DogBeans[1] = new DogBean(4,"Gold",4.5, "Lab", "Jake");
        DogBeans[2] = new DogBean(3, "Brown", 3.7, "Retriever", "Gal");
        DogBeans[3] = new DogBean(4, "Silver", 8.2, "Huskie", "Balto");
        return DogBeans;
    }

    public void displayDogs(DogBean[] DogBeans){
        for(DogBean DogBean : DogBeans) {
            System.out.println(DogBean.toString());
        }
    }

    public static void main(String[] args) {
        Kennel dog = new Kennel();
        DogBean[] allDogs = dog.buildDogs();
        dog.displayDogs(allDogs);
    }
}
